# SmartShoppingCartApplication

Follow the given steps to run the java application.

1) Open the command prompt and goto /SmartShoppingCart/src/ path.

    /src> 
 
2) Compile the application.

    /src> javac com/smart/shooping/SmartShoppingCartApplication.java

3) Run the application by executing below command.
    
    /src> java com/smart/shooping/SmartShoppingCartApplication
    
3) Done.


Follow the steps to run Junit test cases.

 1) Open the command prompt and goto the */SmartShoppingCart/src/* path.
 
    SmartShoppingCart/src>
    
 2) Compile the main test runner class.
    
    SmartShoppingCart/src> javac -cp "F:\SmartShoppingCart\lib\junit-4.12.jar;" com/smart/shopping/unit/test/SmartShoppingCartTestRunner.java
    
 3) Run the test cases by executing below command.
 
    SmartShoppingCart/src> java -cp "F:\SmartShoppingCart\lib\junit-4.12.jar;F:\SmartShoppingCart\lib\hamcrest.jar;" com/smart/shopping/unit/test/SmartShoppingCartTestRunner
                                                                       
 
Thank you!!!