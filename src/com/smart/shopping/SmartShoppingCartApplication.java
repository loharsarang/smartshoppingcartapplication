
package com.smart.shopping;

import java.util.List;
import java.util.Scanner;

import javax.xml.bind.JAXBException;

import com.smart.shopping.model.Product;
import com.smart.shopping.service.SmartShoppingCartService;
import com.smart.shopping.utils.XMLUtil;

/**
 * A core java application to checkout counter for an online retail store that
 * scan products and generates an itemized bill.
 * 
 * @author Sarang Lohar
 *
 */
public class SmartShoppingCartApplication {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws JAXBException {

		SmartShoppingCartService cartService = new SmartShoppingCartService();
		List<Product> availableProducts = XMLUtil.readProductsFromXMLFile();

		do {
			System.out.println("--------------------------------");
			System.out.println("1. List out all the product.");
			System.out.println("2. Add product to cart.");
			System.out.println("3. Remove product from the cart.");
			System.out.println("4. View cart");
			System.out.println("5. Generate Bill.");
			System.out.println("6. Exit.");
			System.out.println("--------------------------------");

			System.out.print("Enter your choice (1, 2, 3, 4, 5 & 6) : ");
			try {
				int choice = Integer.parseInt(scanner.nextLine());
				System.out.println("\n");
				switch (choice) {
				case 1:
					printAllProducts(availableProducts);
					break;

				case 2:
					System.out.print("Enter the Product ID to add in to the cart:");
					Long productIdToAdd = readProductID();
					cartService.addProductToCart(availableProducts, productIdToAdd);
					break;

				case 3:
					System.out.print("Enter the Product ID to remove from the cart:");
					Long productIdToRemove = readProductID();
					cartService.removeProductFromCart(productIdToRemove);
					break;

				case 4:
					cartService.viewCart();
					break;

				case 5:
					cartService.generateBill();
					break;
				case 6:
					System.out.println("Thank you!!!");
					System.exit(0);
					break;
				default:
					System.out.println("\n\nYou have entered incorrect choice number.");
				}
			} catch (Exception e) {
				System.out.println("\n\nPlease enter correct choice number.");
			}
			System.out.println("\n\n");

		} while (true);
	}

	/**
	 * List out all the products present in the system.
	 */
	private static void printAllProducts(List<Product> availableProducts) {
		System.out.printf("List of all products\n");
		System.out.println("--------------------------------");
		System.out.printf("%5s %12s %15s %20s", "ID", "NAME", "PRICE", "CATEGORY\n");
		for (Product product : availableProducts) {
			System.out.format("%5s %14s %14s %15s", product.getId(), product.getName(), "$" + product.getPrice(),
					product.getCategory());
			System.out.println();
		}

		if (availableProducts.size() == 0) {
			System.out.println();
			System.out.printf("%40s", "No product available in the system.\n");
		}
		System.out.println("\n--------------------------------");
	}

	/**
	 * Read product id from command line.
	 * 
	 * @return
	 */
	private static Long readProductID() {
		try {
			long productId = Long.parseLong(scanner.nextLine());
			return productId;
		} catch (Exception e) {
			System.out.print("Please enter a correct product ID:");
			return readProductID();
		}
	}

}
