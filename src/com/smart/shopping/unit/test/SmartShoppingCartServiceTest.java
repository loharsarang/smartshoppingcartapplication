package com.smart.shopping.unit.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.smart.shopping.enumeration.ProductCategory;
import com.smart.shopping.model.Product;
import com.smart.shopping.service.SmartShoppingCartService;

public class SmartShoppingCartServiceTest {

	SmartShoppingCartService smartShoppingCartService = new SmartShoppingCartService();

	@Test
	public void testAddProductToCart() {

		List<Product> availableProducts = getAvailableProduct();

		/** Results */
		assertNotNull(smartShoppingCartService.getCart());
		assertEquals(0, smartShoppingCartService.getCart().size());

		smartShoppingCartService.addProductToCart(availableProducts, 1L);
		smartShoppingCartService.addProductToCart(availableProducts, 2L);
		smartShoppingCartService.addProductToCart(availableProducts, 3L);

		/** Results */
		assertEquals(3, smartShoppingCartService.getCart().size());
		assertNotEquals(1, smartShoppingCartService.getCart().size());

		assertEquals(new Long(1L), smartShoppingCartService.getCart().get(0).getId());
		assertEquals("Product 1", smartShoppingCartService.getCart().get(0).getName());
		assertEquals(ProductCategory.valueOf("A"), smartShoppingCartService.getCart().get(0).getCategory());

		assertEquals(new Long(3L), smartShoppingCartService.getCart().get(2).getId());
		assertEquals("Product 3", smartShoppingCartService.getCart().get(2).getName());
		assertEquals(ProductCategory.valueOf("B"), smartShoppingCartService.getCart().get(2).getCategory());

		System.out.println("Adding product to cart test run successfully.");
	}

	@Test
	public void testRemoveProductFromCart() {

		/** Adding products to cart */
		List<Product> availableProducts = getAvailableProduct();
		smartShoppingCartService.addProductToCart(availableProducts, 1L);
		smartShoppingCartService.addProductToCart(availableProducts, 2L);
		smartShoppingCartService.addProductToCart(availableProducts, 5L);
		assertNotNull(smartShoppingCartService.getCart());
		assertEquals(3, smartShoppingCartService.getCart().size());

		/** Removing one product from the cart */
		smartShoppingCartService.removeProductFromCart(1L);

		/** Results */
		assertEquals(2, smartShoppingCartService.getCart().size());
		assertNotEquals(1, smartShoppingCartService.getCart().size());

		assertEquals(new Long(2L), smartShoppingCartService.getCart().get(0).getId());
		assertEquals("Product 2", smartShoppingCartService.getCart().get(0).getName());
		assertEquals(ProductCategory.valueOf("B"), smartShoppingCartService.getCart().get(0).getCategory());

		/** Removing one product from the cart */
		smartShoppingCartService.removeProductFromCart(2L);

		/** Results */
		assertEquals(1, smartShoppingCartService.getCart().size());
		assertNotEquals(0, smartShoppingCartService.getCart().size());
		assertEquals(new Long(5L), smartShoppingCartService.getCart().get(0).getId());
		assertEquals("Product 5", smartShoppingCartService.getCart().get(0).getName());
		assertEquals(ProductCategory.valueOf("C"), smartShoppingCartService.getCart().get(0).getCategory());

		System.out.println("Removing product from the cart test run successfully.");
	}

	@Test
	public void testCalculateSalesTax() {

		/** Results */
		assertEquals(new Double(12), smartShoppingCartService.calculateSalesTax(120, 10));
		assertEquals(new Double(40), smartShoppingCartService.calculateSalesTax(200, 20));
		assertEquals(new Double(6), smartShoppingCartService.calculateSalesTax(120, 5));
		assertEquals(new Double(78), smartShoppingCartService.calculateSalesTax(650, 12));
		assertEquals(new Double(73.5), smartShoppingCartService.calculateSalesTax(980, 7.5));
		assertEquals(new Double(1656.0000000000002), smartShoppingCartService.calculateSalesTax(12000, 13.8));

		assertNotEquals(new Double(1666.0000000000002), smartShoppingCartService.calculateSalesTax(12000, 13.8));
		assertNotEquals(new Double(9), smartShoppingCartService.calculateSalesTax(120, 5));

		System.out.println("Calculate sales tax test run successfully.");
	}

	/**
	 * Prepare sample products data.
	 * 
	 * @return
	 */
	public List<Product> getAvailableProduct() {
		List<Product> products = new ArrayList<Product>();

		products.add(new Product(1L, "Product 1", 1200.2, ProductCategory.valueOf("A")));
		products.add(new Product(2L, "Product 2", 1400.2, ProductCategory.valueOf("B")));
		products.add(new Product(3L, "Product 3", 1500.2, ProductCategory.valueOf("B")));
		products.add(new Product(4L, "Product 4", 500.2, ProductCategory.valueOf("A")));
		products.add(new Product(5L, "Product 5", 300.2, ProductCategory.valueOf("C")));

		return products;
	}
}