package com.smart.shopping.unit.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * This is helper class to run unit test from command prompt.
 * 
 * @author Sarang Lohar
 */
public class SmartShoppingCartTestRunner {

	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(SmartShoppingCartServiceTest.class);

		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}

		System.out.println("All unit test are run successfully with status: " + result.wasSuccessful());
	}
}