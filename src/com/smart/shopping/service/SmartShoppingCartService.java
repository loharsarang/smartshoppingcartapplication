package com.smart.shopping.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.smart.shopping.model.Product;

public class SmartShoppingCartService {

	/**
	 * Cart to add product that you want. The bill receipt will generate based on
	 * items present in the cart.
	 */
	private List<Product> cart = new ArrayList<Product>();

	public List<Product> getCart() {
		return (List<Product>) Collections.unmodifiableList(cart);
	}

	/**
	 * Add product in the cart.
	 */
	public void addProductToCart(List<Product> availableProducts, Long productId) {

		List<Product> products = findByProductId(availableProducts, productId);

		if (Objects.nonNull(products) && products.size() != 0) {
			cart.add(products.get(0));
			System.out.println("Product added successfully in the cart.");
		} else {
			System.out.println("No product found with ID: " + productId);
		}
	}

	/**
	 * Remove product from the cart.
	 */
	public void removeProductFromCart(Long productId) {

		List<Product> availableProductsInCart = findByProductId(cart, productId);

		if (Objects.nonNull(availableProductsInCart) && availableProductsInCart.size() != 0) {
			cart.remove(availableProductsInCart.get(0));
			System.out.println("Product removed successfully from the cart.");
		} else {
			System.out.println("No product found in the cart with ID: " + productId);
		}
	}

	/**
	 * Print all the products present into the cart.
	 */
	public void viewCart() {
		System.out.printf("View Cart\n");
		System.out.println("--------------------------------");
		System.out.printf("%5s %12s %15s %20s", "ID", "NAME", "PRICE", "CATEGORY\n");
		for (Product product : cart) {
			System.out.format("%5s %14s %14s %15s", product.getId(), product.getName(), "$" + product.getPrice(),
					product.getCategory());
			System.out.println();
		}

		if (cart.size() == 0) {
			System.out.println();
			System.out.printf("%40s", "Shopping cart is empty.\n");
		}
		System.out.println("\n--------------------------------");
	}

	/**
	 * Generate bill receipt based on items present in the cart.
	 */
	public void generateBill() {
		System.out.printf("Bill Receipt");
		System.out.println("\n--------------------------------\n\n");
		Double totalPrice = 0.0;
		Double totalSalesTax = 0.0;

		System.out.printf("%14s %14s %16s %20s", "PRODUCT NAME", "PRICE", "SALES TAX", "FINAL PRICE\n\n");
		for (Product product : cart) {

			double salesTaxOnProduct = calculateSalesTax(product.getPrice(), product.getCategory().getSalesTax());

			double inclusiveTaxPrice = product.getPrice() + salesTaxOnProduct;

			String salesTaxOnProductString = new DecimalFormat("#.##").format(salesTaxOnProduct) + "("
					+ product.getCategory().getSalesTax() + "%)";

			System.out.format("%14s %14s %18s %16s", product.getName(), "$" + product.getPrice(),
					"$" + salesTaxOnProductString, "$" + inclusiveTaxPrice);
			System.out.println();

			totalPrice += inclusiveTaxPrice;
			totalSalesTax += salesTaxOnProduct;
		}

		if (cart.size() == 0) {
			System.out.println();
			System.out.printf("%40s", "Shopping cart is empty.\n");
		}

		System.out.printf("%5s %5s", "\n\nTotal tax:", "$" + new DecimalFormat("#.##").format(totalSalesTax) + "\n");
		System.out.printf("%5s %5s", "Final Amount:", "$" + new DecimalFormat("#.##").format(totalPrice));
		System.out.println("\n--------------------------------");
	}

	public Double calculateSalesTax(double price, double salesTax) {
		return price * (salesTax / 100);
	}

	/**
	 * Filter products based on pass product id.
	 * 
	 * @param products
	 * @param productId
	 * @return list of filtered products.
	 */
	public List<Product> findByProductId(List<Product> products, Long productId) {
		return products.stream().filter(product -> product.getId() == productId).collect(Collectors.toList());
	}

}
