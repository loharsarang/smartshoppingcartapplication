package com.smart.shopping.enumeration;

public enum ProductCategory {
	A(10d), B(20d), C(0d);

	private double salesTax;

	private ProductCategory(double salesTax) {
		this.salesTax = salesTax;
	}

	public double getSalesTax() {
		return this.salesTax;
	}

}
