package com.smart.shopping.utils;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.smart.shopping.model.Product;
import com.smart.shopping.model.Products;

public class XMLUtil {

	private static final String PRODUCTS_FILENAME = "products.xml";

	private static File getProductsJsonFile() {
		return new File(PRODUCTS_FILENAME);
	}

	public static List<Product> readProductsFromXMLFile() throws JAXBException {

		JAXBContext jaxbContext = JAXBContext.newInstance(Products.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		// System.out.println(getProductsJsonFile().getAbsolutePath());

		Products products = (Products) jaxbUnmarshaller.unmarshal(getProductsJsonFile());

		return products.getProducts();

	}
}
